deploy-compose.yml is for usage with docker-compose.

deploy-k8s.yml is for usage with k8s.

# This project has only main branch.

# Main

In the main branch the simplest form of code sits.
No extraction of common code is used to create a generic, re-usable jobs.

# Code-extraction-ci-templates-jobs

In the code-extraction-ci-templates-jobs branch the common code from the pipeline is extracted and placed into ci-templates-poly project. 
Then each microservice is re-using it in its pipeline using include: attribute.

Lesson 7 - 9 - Extract common logic (Job Templates - Part 1) - 54

# K8S

In the K8S branch the microservices are being deployed into K8S cluster.

We create: 

Deployment component(manifest file) for the microservice.
Service component(manifest file) for the microservice.
Those manifest files are generic and re-usable.
We put them into kubernetes folder for each microservice.

In the polyrepo-proj/ci-templates-poly/deploy-k8s.yml job we apply these manifests
using kubectl and kubeconfig.
We also create a secret (type docker-registry) in the cluster with our container registry(GitLab) endpoint and credentials, so that deployment component can deploy PODS using our image defined in deployment manifest.
We also need to specify imagePullSecret in the deployment for deployment to know which secret to use.


kubectl should be installed on the runner.

envsubst is used to replace environment variables, it doesn't work without it.
make sure it is installed on the runner.

If you want to access the app, create a nodePort service (external service) for frontent and then 
browse to shmth like http://tralialia:30838
